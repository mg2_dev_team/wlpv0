const gulp = require('gulp')

const plugins = require('gulp-load-plugins')();
const minify = require('gulp-minify')

// Variables de chemins
const source ='src/'; // dossier de travail
const prod = './Prod'; // dossier à livrer


const jsFiles = {
  source: [
    'src/APP.js',
    'src/Components/Blogscards.js',
    'src/Components/Bookscard.js',
    'src/Components/Header.js',
    'src/Screens/Home.js',
  ]
};


gulp.task('test', function(done) {  
  console.log('Jvais mettre des trucs en prod huhu');
  done()
});

// gulp.task('mini-js', function () {
// return 
//}) 

gulp.task('minify', () => {
  return gulp.src(jsFiles[source] ) 
    .pipe(minify({noSource: true}))
    .pipe(gulp.dest(prod))
})



gulp.task('enprod', function() {
  return gulp.src(source)
    .pipe(minify())
    .pipe(gulp.dest('prod'))

})


gulp.task('default', gulp.series(['minify']));