import React, {Component} from 'react';
import Ressource from '../Components/Ressource';

import Airtable from 'airtable'
import axios from 'axios'
import './Home.css'

Airtable.configure({
    endpointUrl: 'https://api.airtable.com',
    apiKey: ('keyUajiBVtVRhgWX6')
});
const base = Airtable.base('appEXtmEBWk8C3uUn');

let Data = []

class Home extends Component {
  state = {
       product: [],
       content_all : [],
       data_categorie: [],
       tags: [],
       data_tag : [],
       all: true,
       filtercat: false,
       filtercatonly : false,
       filtertag : false,
       research : '',
       results_search: [],
       results_research : false,
       noresult_research : false,
       noresult_message: " Aucune ressource ne correspond à votre recherche, veuillez la reformuler."

    };

GetDataContent =() => {
  axios.get('https://api.airtable.com/v0/appEXtmEBWk8C3uUn/PRODUCT?api_key=keyUajiBVtVRhgWX6')
     .then(result => {
       this.setState({
         content_all: result.data.records,
       })
      })
    }


 GetRessources =() => {
   {base('Product').select().eachPage(function page(records, fetchNextPage) {
      // This function (`page`) will get called for each page of records.
      records.map(record => Data.push(record))
      //Ressources.push(record)
      // This table contains all the records of knowledge base
      //console.log('tableau ressource', Data)
      // To fetch the next page of records, call `fetchNextPage`.
      // If there are more records, `page` will get called again.
      // If there are no more records, `done` will get called.
      fetchNextPage();
    } 
    ,function done(err) {
      console.log('henlo');
      if (err)
      { console.error(err); return; }
    })
   }
   setTimeout(this.StockData(Data), 1000)
 }

  StockData = (Data) => {
    this.setState({ product : Data})
  }

    handleClickCat = async (categorie) => {
    // console.log("la catégorie séléctionnée est ", categorie)
      const tabRecord = []
      // On parcours le tableau des données, en excluant celles qui n'ont pas de cétagorie
      // Si une erreur de remplissage est présente dans la table ( champs / catégorie / tags ) ca plantera ici --- vérifier airtable en filtrant les résultats avec la condition If 'Catégorie' is empty.
        const research = Data.filter(result => result.fields.Catégorie.map(cat => {
        // Si la catégorie de cette entrée correspond à celle séléctionnée par l'utilisateur on copie l'entrée dans le tableau
       if (cat === categorie) {        
        tabRecord.push(result)
        // On parcours le tableau des entrées catégorisées et on exclue les ressources sans Tags
        const tabletags = tabRecord.filter(tag => tag.fields.Tag)
        let tags = tabletags.map(res => res.fields.Tag)
        // On met les tableaux de tags imbriqués à plat 2 fois
        const cleantag = [tags.flat()];
        const unique = cleantag.flat()
        // On stock dans uniqtags la première occurence de chaque tags recencés même si celui-ci apparait plusieurs fois
        const uniqtags = [...new Set(unique)]
        this.setState({tags: uniqtags})
        console.log("les tags uniques’", uniqtags )
        //console.log('ce fucking tableau de la mort', tabRecord)
        this.setState({data_categorie: tabRecord , all: false, filtercat: true, results_research: false, filtercatonly: true, noresult_research : false,
        })
        }
       }))
      }

 // fonction qui recherche les articles liés au tag séléctionné
  handleClickTag = async (tag) => {
    console.log("ici pollybucket")
    const tabTag = []
    const tagAll = await this.state.data_categorie.filter(subject => subject.fields.Tag)
    await tagAll.filter(res => res.fields.Tag.filter(thistag => {
    if (thistag === tag) {
       tabTag.push(res)
    }
    }))
    this.setState({data_tag: tabTag, all :false, filtertag: true, filtercatonly: false, results_research: false, noresult_research : false, })
      console.log("Curly", this.state.data_tag)
  }

  // handleSearch = (e) => {
  //   const value = e.target.value
  //   this.OnTextResearchChanged(value)
  // }

  handleSearch = () => {
    const research = document.getElementById("researchvalue").value
    //console.log("ici", research)
    this.ShowResults(research)
  }

  ShowResults = async(research) => {
    const SuggestionsContent = [];
    if (research.length >= 0) {
      console.log("ici la longueur",research.length)
     //const regex = new RegExp (`${value}`);

     const obj = this.state.product.map(record => record)
     await obj.filter(result =>  {
      const mystring = `${research.toLowerCase()}`;
      const myTitre = result.fields.Titre.toLowerCase()
      // const myTags = result.fields.Tag.flat()
      // console.log(myTags)
      let myDescription = result.fields.Description

      if (myDescription) {
        myDescription= result.fields.Description.toLowerCase()
      } else {
        myDescription = ""
      }
       console.log(mystring)
      
        if ((myDescription.search(mystring) !== -1) || (myTitre.search(mystring) !== -1)){
          SuggestionsContent.push(result)
        }
        
    })
    if(SuggestionsContent.length === 0 ) {
      //console.log("cest vidouille ici", SuggestionsContent)
      this.setState({
        noresult_research : true,
        filtercatonly: false,
        results_research: false,
      })
    } else if (SuggestionsContent.length > 0) {
       this.setState({
        results_search: SuggestionsContent, research : research, results_research : true, all : false, filtercat: false, filtertag: false, filtercatonly: false, noresult_research : false,
      })
      console.log('pitié maaaaaaarche', this.state.results_search)
    }

       }
     }


  componentDidMount() {
    this.GetRessources()
    this.GetDataContent()
  }

  
  render() {

    const categories= [
      "Design",
      "Data",
      "Organisation et Process",
      "Tech",
      "Softskills",
      "Product Management",
      "Growth Marketing",
      "Stratégie",
    ]

    return (
      <>
      <div className="Separation"></div>
      <div className="xploration">
        <h4>Explorez la base de connaissances We Love Product !</h4>
        <p>Je recherche une ressource à propos de :</p>
        <div className="research-box">
        <input className="research" type="text" id="researchvalue" name="search" placeholder="Rechercher" /><button type="submit" id="research" onClick={this.handleSearch}>Rechercher</button>
        </div>
      </div>
      <div className="nav-categorie">
        {/* Affiche un bouton par catégorie représentée dans la table Product et définit qu'au clic sur l'une d'elles on effectue un filtre dans le résulat globale des ressources */}
        {categories.map(categorie => <div className="categorie"><input type="submit" key={categorie} className="button-cat" value={categorie} onClick={() => this.handleClickCat(categorie)}/></div> )}
     </div>

      {/* Si la state all est true, ça signifie que l'utilisateur n'a pas encore séléctionné de catégorie ni fait de recherche alors on met du contenu en avant sans disctinction de catégorie */}
      {this.state.all ? this.state.content_all.map(content => <Ressource {...content.fields} key={content.id}/> ) : null }

      {/* Si la state activeresearch est true, l'utilisateur a éffectué une recherche, alors on affiche les résultats */}
      {this.state.results_research ? this.state.results_search.map(content => <Ressource {...content.fields} key={content.id} /> ) : null}

      {this.state.noresult_research ? <p>{this.state.noresult_message}</p> : null }

       {/* Affiche la liste des tags présents dans la catégorie séléctionnée pour affiner la recherche */}
      {this.state.filtercat ? this.state.tags.map(tag => <input type="submit"  key={tag} className="button-tag" value={tag} onClick={() => this.handleClickTag(tag)}/> ) : null }<br/>

      {/* Si l'utilisateur a séléctionner une catégorie mais pas encore de tags, on affiche les résultats de la catégorie */}
      {this.state.filtercatonly ? this.state.data_categorie.map(content => <Ressource {...content.fields} key={content.id} /> ) : null}

      {/* Si la state filtertag est true ça signifie que l'utilisateur à séléctionné à une catégorie et un tag, alors on affiche le résultat correspondant aux 2 niveaux de recherche, si juste une catégorie à été séléctionner, on affiche les résultats de la bonne catégorie sans disctinction de tag */}
      {this.state.filtertag ? this.state.data_tag.map(ressource => <Ressource {...ressource.fields} key={ressource.id} /> ) : null }
      </>
    );
  }
}

export default Home