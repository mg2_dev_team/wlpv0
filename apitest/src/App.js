import React from 'react';
import {Switch, Route} from 'react-router-dom'
import Favicon from 'react-favicon';
import favicon from './Components/wlpicon.png'

import Header from './Components/Header';
import Home from './Screens/Home'

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';



function App() {
  return (
    <div className="App">
      <Favicon url={favicon} />
      <Header />
      <Home />
      {/* <Switch>
        <Route exact path="/" component={Home} />
         <Route path="/research" component={Result} />
      </Switch> */}
    </div>
  );
}

export default App;
