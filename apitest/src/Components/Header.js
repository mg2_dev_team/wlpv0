import React, { Component } from 'react'
import Fullbg from './Fullbg_white.jpg'

import './Header.css'

class Header extends Component {
  render() {
    return (
      <>
        <div className="container-header">
          <img className="logowlp" src={Fullbg} alt="we love product" />
          <div className="nav">
            <a href="http://www.weloveproduct.com/" className="nav-item" > Communauté </a> 
            <a href="/" className="nav-item"> Base de connaissances </a>
          </div> 
        </div>

      </>
    )
  }
}

export default Header

