import React from 'react'
import logolink from './Logolink.png'
import './Ressource.css'

  const Ressource = ({Titre, Catégorie, Image,  Date, Description, Tag, Typologie,  Langue, URL, id}) => (
    <div className="containerdata">
       <div key={id}>
       <a href={URL}>
        {/* <div className="header-img">
           <img className="logo" src={Image} alt={Titre}/>
        </div> */}
        <div className="card-body">
            <h4 className="title">{Titre}</h4>
        <div>
            <p className="infos">  {Typologie} </p>   
        </div>
      </div>
      </a> 
    </div>
  </div>
  );

export default Ressource
